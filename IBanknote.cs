﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_06.ChainOfResponsibility
{
    public interface IBanknote
    {
        CurrencyType Currency { get; }
        string Value { get; }
    }
}
