﻿using System;
using System.Text.RegularExpressions;

namespace Example_06.ChainOfResponsibility
{
    public abstract class AbstractHandlerBase : BanknoteHandler
    {
        public override string Evaluate(string requestValue)
        {
            if (!requestValue.Contains(CurrencySign))
            {
                return base.Evaluate(requestValue);
            }

            int iRequestValue = ParseCashEvalRequest(requestValue);
            int banknoteCount = iRequestValue / Value;

            if (banknoteCount <= 0)
            {
                return base.Evaluate($"{iRequestValue}{CurrencySign}");
            }

            int ejectedSum = banknoteCount * Value;
            iRequestValue -= ejectedSum;

            return $"{ Value }x{ banknoteCount } + { base.Evaluate($"{iRequestValue}{CurrencySign}") } ";
        }

        protected AbstractHandlerBase(BanknoteHandler nextHandler) : base(nextHandler)
        {
        }
    }
}
