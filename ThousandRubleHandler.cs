﻿

namespace Example_06.ChainOfResponsibility
{
    public class ThousandRubleHandler : RubleHandlerBase
    {
        protected override int Value => 1000;

        public ThousandRubleHandler(BanknoteHandler nextHandler) : base(nextHandler)
        { }
    }
}
