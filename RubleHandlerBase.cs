﻿
namespace Example_06.ChainOfResponsibility
{
    public abstract class RubleHandlerBase : AbstractHandlerBase
    {
        protected override string CurrencySign => "Р";

        protected RubleHandlerBase(BanknoteHandler nextHandler) : base(nextHandler)
        {
        }
    }
}
