﻿using System;
using System.Text.RegularExpressions;

namespace Example_06.ChainOfResponsibility
{
    public abstract class BanknoteHandler
    {
        private readonly BanknoteHandler _nextHandler;

        protected abstract string   CurrencySign { get; }
        protected abstract int      Value        { get; }

        protected BanknoteHandler(BanknoteHandler nextHandler)
        {
            _nextHandler = nextHandler;
        }

        protected int ParseCashEvalRequest(string requestValue)
        {
            string sValue = Regex.Replace(requestValue, $@"[^\d\-]", "");
            int result = default(int);
            try
            {
                result = Convert.ToInt32(string.IsNullOrEmpty(sValue) ? "0" : sValue);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Cash request parse error: " + ex.Message);
            }
            
            return Math.Max(result, 0);
        }

        public bool Validate(string banknote)
        {
            if (banknote.Equals($"{Value}{CurrencySign}"))
            {
                return true;
            }
            return _nextHandler != null && _nextHandler.Validate(banknote);
        }

        public virtual string Evaluate(string requestValue)
        {
            int iRequestValue = ParseCashEvalRequest(requestValue);
            if (_nextHandler == null)
            {
                if (iRequestValue != 0)
                {
                    return $"{iRequestValue} Non-valid =(";
                }

                return "0";
            }

            return _nextHandler.Evaluate(requestValue);
        }
    }
}
