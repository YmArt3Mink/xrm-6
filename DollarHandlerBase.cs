﻿
namespace Example_06.ChainOfResponsibility
{
    public abstract class DollarHandlerBase : AbstractHandlerBase
    {
        protected override string CurrencySign => "$";

        protected DollarHandlerBase(BanknoteHandler nextHandler) : base(nextHandler)
        {
        }
    }
}
