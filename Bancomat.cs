﻿using System.Linq;
using System.Collections;
using System.Text.RegularExpressions;

namespace Example_06.ChainOfResponsibility
{
    public class Bancomat
    {
        private readonly BanknoteHandler _handler;

        public Bancomat()
        {
            _handler = new TenRubleHandler(null);
            _handler = new ThousandRubleHandler(_handler);
            _handler = new TenDollarHandler(_handler);
            _handler = new FiftyDollarHandler(_handler);
            _handler = new HundredDollarHandler(_handler);
        }

        public bool Validate(string banknote)
        {
            return _handler.Validate(banknote);
        }

        public string Evaluate(string requestValue)
        {
            string result = _handler.Evaluate(requestValue);

            string[] ignoreList = new string[]
            {
                "x1 ",
                " + 0"
            };
            ignoreList
                .ToList()
                .ForEach(ignoreSubstr =>
                {
                    result = result.Replace(ignoreSubstr, " ");
                });

            return result;
        }
    }
}
